﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(EnemySpawner))]
public class EnemySpawnerEditor : Editor
{
    int myValue;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUILayout.Space(20f);


        EditorGUI.BeginDisabledGroup(Application.isPlaying == false);
        if (GUILayout.Button("Create Tank"))
        {
            CreateNewEnemy();
        }
        EditorGUI.EndDisabledGroup();
    }

    private void CreateNewEnemy()
    {
        UnityEngine.Object uobject = this.target;
        EnemySpawner enemy = (EnemySpawner)uobject;
        enemy.Spawn();
    }


    // EnemeSpawner : MonoBehaviour : Behaviour : Component : UnityEngine.Object : object
}
