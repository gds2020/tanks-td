﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class TowerManager : MonoBehaviour
{
    public static TowerManager Manager { get; private set;}

    public Tower[] AllTowers {get; private set;}


    void Awake()
    {
        if (TowerManager.Manager != null)
            throw new System.InvalidProgramException("singletone violation");
        TowerManager.Manager = this;
        AllTowers = this.GetComponentsInChildren<Tower>();
    }
}
