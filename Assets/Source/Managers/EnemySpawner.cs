﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class EnemySpawner : MonoBehaviour
{
    private static EnemySpawner single;
    static public EnemySpawner Spawner
    {
        get
        {
            return single;
        }
    }




    public GameObject protype;
    public Path path;


    public int count;
    public float delay;
    public float beginSwawnAfter;

    List<Enemy>  enemyList;
    private int currentCount;


    public Enemy[] EnemyArray
    {
        get
        {
            Enemy[] arr = enemyList.ToArray();
            return arr;
        }
    }


    public void RemoveFromList(Enemy me)
    {
        Debug.Assert(enemyList.Contains(me), "not in list");
        this.enemyList.Remove(me);
    }


    private void Awake()
    {
        Debug.Assert(single == null, "not a singleton");
        single = this;
    }

    private void Start()
    {
        IEnumerator coroutine = SpawnCoroutine();
        enemyList = new List<Enemy>();
        this.StartCoroutine(coroutine);
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            path.enabled = !path.enabled;
    }


    public void Spawn()
    {
        GameObject clone = GameObject.Instantiate(protype);
        Enemy enemyComponnent = clone.GetComponent<Enemy>();
        Debug.Assert(enemyComponnent != null, "component not found");
        enemyList.Add(enemyComponnent);
        //NOTE: remove from list on destroy
        enemyComponnent.Initialize();
        enemyComponnent.SetPath(path);
    }


    private IEnumerator SpawnCoroutine()
    {
        yield return new WaitForSeconds(beginSwawnAfter);
        while (currentCount < count)
        {
            Spawn();
            currentCount++;
            yield return new WaitForSeconds(delay);
        }
    }

    //............................................................................................//


}
