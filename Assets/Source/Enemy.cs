﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class Enemy : UnitBase
{
    protected UnitMover mover;

    protected Path path;

    protected int targetWaypointIndex;


    private bool isDestroyedByDamage;



    public void Initialize()
    {
        mover = GetComponent<UnitMover>();
        if (mover == null)
            Debug.LogError("can't find component");
    }

    protected void OnPathExhausted()
    {
        // HACK
        Debug.Log("fix this hack");
        OnHealthExhausted();
    }

    private void OnDestroy()
    {
        if (isDestroyedByDamage) return;
        EnemySpawner.Spawner.RemoveFromList(this);
    }


    public void SetPath(Path p)
    {
        Debug.Assert(p != null, "path is null");
        path = p;
        mover.InitializeStartPosition(p.GetPosition(0), p.GetPosition(1));
    }



    protected override void OnHealthExhausted()
    {
        isDestroyedByDamage = true;
        EnemySpawner.Spawner.RemoveFromList(this);
        GameObject.Destroy(this.gameObject);
    }

    protected override void FindNeareastTarget()
    {
        Tower[] allTowers = TowerManager.Manager.AllTowers;
        float minDistance = float.MaxValue;
        Tower closestTower = null;

        for (int i = 0; i < allTowers.Length; i++)
        {
            float distance = (this.transform.position - allTowers[i].transform.position).magnitude;
            if (distance < minDistance)
            {
                minDistance = distance;
                closestTower = allTowers[i];
            }
        }

        this.target = minDistance <= this.Radius ? closestTower : null;
    }
}
