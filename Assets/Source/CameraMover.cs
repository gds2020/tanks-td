﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;




[RequireComponent(typeof(Camera))]
public class CameraMover : MonoBehaviour
{
    [SerializeField]
    [Tooltip("This controls how fast the drag is applyied to camera")]
    float multiplier = 1f;

    [SerializeField]
    float[] zoomLevels;


    Vector3 prevMousePos;
    Camera  cam;
    int currentZoomIndex;



    void Awake()
    {
        cam = this.GetComponent<Camera>();
        Debug.Assert(cam.orthographic);
        Debug.Assert(zoomLevels.Length >= 2);
        currentZoomIndex = 1;
        cam.orthographicSize = zoomLevels[currentZoomIndex];
    }



    void LateUpdate()
    {
        Vector3 positionDelta = Input.mousePosition - prevMousePos;
        if (Input.GetMouseButton(1))
        {
            positionDelta.z = 0f;
            transform.localPosition += -positionDelta * multiplier * 0.01f;
        }
        prevMousePos = Input.mousePosition;


        if (Input.mouseScrollDelta != Vector2.zero)
        {
            bool isMoveUp = Input.mouseScrollDelta.y < 0f;
            ZoomCamera(isMoveUp);
        }
    }


    private void ZoomCamera(bool zoomUp)
    {
        currentZoomIndex += zoomUp ? 1 : -1;
        currentZoomIndex = Mathf.Clamp(currentZoomIndex, 0, zoomLevels.Length - 1);
        float cameraSize = zoomLevels[currentZoomIndex];
        cam.orthographicSize = cameraSize;
    }

}
