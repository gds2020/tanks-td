﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public abstract class UnitBase : MonoBehaviour
{
    [SerializeField] Transform gunSlotXF;
    [SerializeField] bool turnWeaponToTarget;
    [SerializeField] float timeEstimationAccuracy;
    [SerializeField] protected float health = 100f;

    protected UnitBase target;


    //......................................................................................PROPERTY

    public bool HasTarget => target != null;
    public bool HasWeapon => Weapon != null;

    public float Radius => Weapon.Radius;

    protected Weapon Weapon
    {
        get
        {
            Debug.Assert(gunSlotXF != null, "not assigned gun slot", this);
            if (weaponBakingField__ == null)
                weaponBakingField__ = gunSlotXF.GetComponentInChildren<Weapon>();
            return weaponBakingField__;
        }
    }
    private Weapon weaponBakingField__;


    //.........................................................................................UNITY

    protected virtual void Update()
    {
    }

    private bool IsNotInsideRadius()
    {
        float distance = (transform.position - target.transform.position).magnitude;
        return distance > Radius;
    }

    //........................................................................................PUBLIC

    public virtual void TakeDamage(float ammount)
    {
        health = Mathf.Max(health, 0f);
        if (health <= 0f) OnHealthExhausted();
    }

    //......................................................................................ABSTRACT

    protected abstract void OnHealthExhausted();

    protected abstract void FindNeareastTarget();

    //.......................................................................................PRIVATE

    protected bool CanFire()
    {
        if (!HasWeapon) return false;
        if (!HasTarget) FindNeareastTarget();
        if (HasTarget && IsNotInsideRadius()) target = null;
        if (!HasTarget) return false;
        if (!Weapon.CanFire) return false;
        return true;
    }

    protected Vector3 GetFirePosition()
    {
        Debug.Assert(target != null);
        Debug.Assert(Weapon != null);

        UnitMover targetMover = target.GetComponent<UnitMover>();
        if (targetMover == null)
            return target.transform.position;

        float distance = (transform.position - target.transform.position).magnitude;
        float time = Weapon.GetProjectileTime(distance);


        Vector3 preemptivePosition = targetMover.GetPreemptivePosition(time);
        return this.GetInnacuratePosition(preemptivePosition);
    }


    private void TurnWeaponToTarget()
    {
        throw null; // refactor this
        Vector3 vectorToTarget = target.transform.position - this.transform.position;
        Vector3 directionToTarget = vectorToTarget.normalized;
        gunSlotXF.rotation = Quaternion.LookRotation(directionToTarget, -Vector3.forward);
    }



    private bool IsAimingAtTarget()
    {
        Vector3 dirToTarget = (this.target.transform.position - this.transform.position).normalized;
        Vector3 weaponDir = this.gunSlotXF.forward;
        float angle = Vector3.Angle(dirToTarget, weaponDir);
        return angle < 0.1f;
    }


    private Vector3 GetInnacuratePosition(Vector3 fixedPosition)
    {
        return fixedPosition;
    }



    //........................................................................................EDITOR

    //void OnDrawGizmosSelected()
    void OnDrawGizmos()
    {
        if (!this.HasWeapon) return;
        Gizmos.color = new Color(0f, 1f, 0.2f);
        UnityEditor.Handles.DrawWireArc(this.transform.position, this.transform.up, this.transform.right, 360f, Weapon.Radius);
    }

}
