﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum EVisualState
{
    MoveState,
    FireState,
}

public class InfantryEnemy : Enemy
{
    [SerializeField] float aimTime;
    [SerializeField] float delayAfterFire;
    [SerializeField] GameObject visualMoveObject;
    [SerializeField] GameObject visualFireObject;

    private bool isFiring;

    public override void TakeDamage(float ammount)
    {
        this.health -= ammount;
        this.health = Mathf.Max(this.health, 0f);
        base.TakeDamage(ammount);
    }


    public void ChangeState(EVisualState visualType)
    {
        switch (visualType)
        {
            case EVisualState.MoveState:
                visualFireObject.SetActive(false);
                visualMoveObject.SetActive(true);
                break;

            case EVisualState.FireState:
                visualFireObject.SetActive(true);
                visualMoveObject.SetActive(false);
                break;

            default:
                throw new System.NotImplementedException("not implemented for type: " + visualType);
        }
    }


    void Awake()
    {
        ChangeState(EVisualState.MoveState);
    }

    protected override void Update()
    {
        if (this.CanFire() && !isFiring)
        {
            this.StartCoroutine( FireAtTargetCR() );
        }

        if (targetWaypointIndex != -1 && mover.IsOnTarget() == true)
        {
            targetWaypointIndex++;
            Debug.LogFormat("target waypoint changed to {0}", targetWaypointIndex);
            if (targetWaypointIndex < path.GetCount())
            {
                Vector3 nextWaypointLocation = path.GetPosition(targetWaypointIndex);
                mover.MoveTo(nextWaypointLocation);
            }
            else
            {
                targetWaypointIndex = -1;
                this.OnPathExhausted();
            }
        }
    }

    private IEnumerator FireAtTargetCR()
    {
        Debug.Assert(this.Weapon != null);
        Debug.Assert(this.Weapon.CanFire);

        isFiring = true;
        float oldSpeed = this.mover.maxMoveSpeed;
        this.mover.maxMoveSpeed = 0f;
        this.mover.LookAt(this.target.transform.position);

        ChangeState(EVisualState.FireState);
        yield return new WaitForSeconds(aimTime);
        Vector3 firePos = this.GetFirePosition();
        this.Weapon.Fire(firePos);

        yield return new WaitForSeconds(delayAfterFire);
        ChangeState(EVisualState.MoveState);

        this.mover.maxMoveSpeed = oldSpeed;
        isFiring = false;
    }


    [ContextMenu("Change state to MOVE")]
    private void ForceSetStateMove()
    {
        ChangeState(EVisualState.MoveState);
    }


    [ContextMenu("Change state to FIRE")]
    private void ForceSetStateFire()
    {
        ChangeState(EVisualState.FireState);
    }
}
