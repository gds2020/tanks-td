﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class UnitMover : MonoBehaviour
{
    public float maxMoveSpeed = 1f;

    protected Vector3 targetPosition;                                // used by tank to rotate tower
    protected Quaternion orientationToTarget;                          // TODO: check this protected


    private Vector3 previousPosition;
    private float currentSpeed;

    //.........................................................................................UNITY

    private void Awake()
    {
        targetPosition = transform.position;
    }

    void Update()
    {
        float positionDelta = (previousPosition - transform.position).magnitude;
        currentSpeed = positionDelta / Time.deltaTime;
        previousPosition = transform.position;

        if (IsOnTarget())
            return;

        Vector3 currPos = Vector3.MoveTowards(transform.position, targetPosition, maxMoveSpeed * Time.deltaTime);
        transform.position = currPos;
    }

    //........................................................................................PUBLIC

    public virtual void MoveTo(Vector3 position)
    {
        targetPosition = position;
        Vector3 vectorToTarget = targetPosition - transform.position;
        orientationToTarget = Quaternion.LookRotation(vectorToTarget.normalized, -Vector3.forward);
        this.transform.rotation = orientationToTarget;
    }


    public bool IsOnTarget()
    {
        Vector3 current = transform.position;
        Vector3 target = targetPosition;

        current.z = 0f;
        target.z = 0f;

        Vector3 v = current - target;
        return v.sqrMagnitude < 0.00001;
    }


    public Vector3 GetPreemptivePosition(float timeDelta)
    {
        float distance = currentSpeed * timeDelta;
        Vector3 positionOffest = transform.forward * distance;
        Vector3 preemptivePostion = transform.position + positionOffest;
        return preemptivePostion;
    }


    public void InitializeStartPosition(Vector3 origin, Vector3 lookPosition)
    {
        LookAt(lookPosition);
        this.transform.position = origin;
        previousPosition = this.transform.position;
        targetPosition = previousPosition;
    }

    public void LookAt(Vector3 position)
    {
        Vector3 lookDirection = (position - this.transform.position).normalized;
        this.transform.rotation = Quaternion.LookRotation(lookDirection, -Vector3.forward);
    }

}
