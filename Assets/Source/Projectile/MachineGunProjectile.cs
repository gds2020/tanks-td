﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class MachineGunProjectile : Projectile
{
    protected override void OnProjectileExlode(Collider other)
    {
        Enemy myEnemy = null;
        myEnemy = other.GetComponent<Enemy>();
        if (myEnemy == null)
        {
            Debug.Log("explosion and sound plays");
            return;
        }

        myEnemy.TakeDamage(this.Damage);
    }
}
