﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class MortarProjectile : Projectile
{
    [SerializeField]
    private float explosionRadius = 1f;


    protected override void OnProjectileExlode(Collider other)
    {
        Enemy[] allEnemies = EnemySpawner.Spawner.EnemyArray; // rvalue
        for (int i = 0; i < allEnemies.Length; i++)
        {
            Enemy currentEnemy = allEnemies[i];
            float distance = (this.transform.position - currentEnemy.transform.position).magnitude;
            if (distance < explosionRadius)
            {
                float explodeMultiplied = 1f - distance / explosionRadius;
                currentEnemy.TakeDamage(explodeMultiplied * this.Damage);
                // TODO: display damage in UI
            }
        }
    }
}
