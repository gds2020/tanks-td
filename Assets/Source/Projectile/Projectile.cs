﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public abstract class Projectile : MonoBehaviour
{
    [SerializeField]
    float damage = 20f;
    [SerializeField]
    float speed = 1f;

    private bool hasTarget;
    private Vector3 endPosition;

    public float Damage => damage;
    public float Speed  => speed;

    public void SetEndPosition(Vector3 wolrdPosition)
    {
        hasTarget = true;
        endPosition = wolrdPosition;
    }

    void Update()
    {
        if (!hasTarget)
            return;

        this.transform.LookAt(endPosition, -Vector3.forward);

        Vector3 currentPosition = Vector3.MoveTowards(this.transform.position, endPosition, speed * Time.deltaTime);
        this.transform.position = currentPosition;

        if (currentPosition == endPosition)
        {
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        OnProjectileExlode(other);
        Destroy(this.gameObject);
    }


    protected abstract void OnProjectileExlode(Collider other);

}
