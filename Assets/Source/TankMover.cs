﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class TankMover : UnitMover
{
    public float minMoveSpeed = 0.1f;
    public float acceleration = 0.1f;
    public float breakAcceleration = 0.1f;
    public float rotateSpeed = 10f;

    private float speed;
    private bool isRotating;


    void Update()
    {
        if (IsOnTarget())
            return;

        Vector3 vectorToTarget = targetPosition - transform.position;

        if (isRotating)
            FaceDirection(vectorToTarget.normalized);
        else
            MovoToPosition(targetPosition, vectorToTarget.magnitude);
    }


    public override void MoveTo(Vector3 position)
    {
        targetPosition = position;
        Vector3 vectorToTarget = targetPosition - transform.position;
        orientationToTarget = Quaternion.LookRotation(vectorToTarget.normalized, -Vector3.forward);
        isRotating = true;
    }


    private void FaceDirection(Vector3 direction)
    {
        Quaternion targetRotation = Quaternion.LookRotation(direction, -Vector3.forward);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotateSpeed * Time.deltaTime);
        isRotating = Quaternion.Angle(transform.rotation, targetRotation) > 0.1f;
    }


    private void MovoToPosition(Vector3 targetPosition, float distanceToTarget)
    {
        // acceleration = (V^2)/(2L); V^2 velocity squared, L - distance
        float breakDistance = speed * speed / (2f * breakAcceleration);

        if (distanceToTarget > breakDistance)
            speed = Mathf.Min(speed + acceleration * Time.deltaTime, maxMoveSpeed);
        else
            speed = Mathf.Max(speed - breakAcceleration * Time.deltaTime, minMoveSpeed);

        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
    }
}
