﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class TankEnemy : Enemy
{
    [SerializeField] float armor;


    public override void TakeDamage(float amount)
    {
        // NOTE: make some damage pass through the armor
        float actualDamage = 0f;
        actualDamage = Mathf.Max(amount - armor, 0f);
        this.health -= actualDamage;
        base.TakeDamage(amount);
    }
}
