﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public abstract class Weapon : MonoBehaviour
{
    public float coolDown;
    [SerializeField]
    Projectile projectilePrototype;
    [SerializeField]
    Transform gunMuzzlePoint;
    [SerializeField]
    float shellSpreadRadius = 1f;
    [SerializeField]
    float radius = 10f;


    float prevFireTime = -1000f;

    public bool CanFire
    {
        get
        {
            float timeDelta = Time.time - prevFireTime;
            return timeDelta > coolDown;
        }
    }

    public float Radius => radius;


    public virtual void Fire(Vector3 toPosition)
    {
        if (!CanFire)
            throw new System.InvalidOperationException("cooldown not consumed");

        prevFireTime = Time.time;
        Vector3 actualFirePoint = toPosition;
        actualFirePoint.x += Random.Range(-1f, 1f) * shellSpreadRadius;
        actualFirePoint.y += Random.Range(-1f, 1f) * shellSpreadRadius;
        this.CreateProjectile().SetEndPosition(actualFirePoint);
    }


    public float GetProjectileTime(float distance)
    {
        float speed = projectilePrototype.Speed;
        float time = distance / speed;
        return time;
    }


    protected Projectile CreateProjectile()
    {
        Projectile clone = Object.Instantiate(projectilePrototype);
        clone.transform.position = gunMuzzlePoint.position;
        return clone;
    }

}
