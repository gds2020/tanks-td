﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class Tower : UnitBase
{
    float turnFactor;



    void Awake()
    {
        turnFactor = 1f;
    }


    protected override void OnHealthExhausted()
    {
        Destroy(this.Weapon.gameObject);
    }


    protected override void FindNeareastTarget()
    {
        Enemy[] allEnemies = EnemySpawner.Spawner.EnemyArray;
        float minDistance = float.MaxValue;
        Enemy closestEnemy = null;

        for (int i = 0; i < allEnemies.Length; i++)
        {
            float distance = (this.transform.position - allEnemies[i].transform.position).magnitude;
            if (distance < minDistance)
            {
                minDistance = distance;
                closestEnemy = allEnemies[i];
            }
        }

        this.target = minDistance <= this.Radius ? closestEnemy : null;
    }


    public override void TakeDamage(float ammount)
    {
        this.health = this.health - ammount / 10f;
        base.TakeDamage(ammount);
    }

}