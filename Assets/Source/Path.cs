﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Path : MonoBehaviour
{
    private Vector3[] points;


    private void Awake()
    {
        points = new Vector3[this.transform.childCount];

        for (int i = 0; i < points.Length; i++)
        {
            Transform childTransform = this.transform.GetChild(i);
            points[i] = childTransform.position;
        }
    }


    public int GetCount()
    {
        return points.Length;
    }

    public Vector3 GetPosition(int waypointIndex)
    {
        return points[waypointIndex];
    }


    void OnDrawGizmos()
    {
        int count = this.transform.childCount;

        Gizmos.color = Color.cyan;
        for (int i = 1; i < count; i++)
        {
            Transform currXf = this.transform.GetChild(i);
            Transform prefXf = this.transform.GetChild(i-1);
            Gizmos.DrawLine(currXf.position, prefXf.position);
        }

    }

    void OnDrawGizmosSelected()
    {
        int count = this.transform.childCount;
        Vector3 startPosition = this.transform.GetChild(0).position;
        Vector3 endPosition = this.transform.GetChild(count-1).position;

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(startPosition, 0.2f);
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(endPosition, Vector3.one * 0.3f);
    }

}
